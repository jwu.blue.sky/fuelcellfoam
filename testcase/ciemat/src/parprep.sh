#!/bin/bash

# edit system/decomposeParDict for the desired decomposition
NPROCS=`cat system/decomposeParDict | grep "numberOfSub" | awk '{print $2}' | sed -e 's/;//g'`

# decompose the global mesh
rm -rf processor*
decomposePar -force

mpirun -np $NPROCS splitMeshRegions -cellZonesOnly -overwrite -parallel 

cp -r 0/* processors$NPROCS/0/air/
cp -r 0/* processors$NPROCS/0/fuel/
cp -r 0/* processors$NPROCS/0/
cp -r 0/* processors$NPROCS/0/interconnect/
cp -r 0/* processors$NPROCS/0/electrolyte/
rm processors$NPROCS/0/*/k processors$NPROCS/0/interconnect/rho

# region sets are lost in decomposition: replace them now
#
rm -rf processor*/constant/polyMesh/sets processor*/constant/polyMesh/*Zones


# remove the 1 directory and create the fluid zones
#
rm -r processor*/1

# air zones
#
mpirun -np $NPROCS topoSet -region air -parallel
mpirun -np $NPROCS setsToZones -region air -parallel

# fuel zones
#
mpirun -np $NPROCS topoSet -region fuel -parallel
mpirun -np $NPROCS setsToZones -region fuel -parallel

# electrolyte zones
#
mpirun -np $NPROCS topoSet -region electrolyte -parallel
mpirun -np $NPROCS setsToZones -region electrolyte -parallel

# interconnect zones
#
#mpirun -np $NPROCS topoSet -region interconnect
#mpirun -np $NPROCS setsToZones -region interconnect

# cleanup
./src/cleanup.sh
