#!/bin/bash

NPROCS=`cat system/decomposeParDict | grep "numberOfSub" | awk '{print $2}' | sed -e 's/;//g'`

# remove field files from regions (including global region) that don't use them
# set noGlobal is removed from global, etc.

FIELDS="    s diffH2 diffO2 k p pd rho T U YH2 YH2O YN2 YO2"
noGlobal="  s diffH2 diffO2   p pd rho   U YH2 YH2O YN2 YO2 "
noAir="     diffH2 k YH2 "
noFuel="    diffO2 k YN2 YO2 "

cd processors$NPROCS
cd 0; rm $noGlobal; cd ..
cd 0/air; rm $noAir; cd ../..
cd 0/fuel; rm $noFuel; cd ../..
cd ..
