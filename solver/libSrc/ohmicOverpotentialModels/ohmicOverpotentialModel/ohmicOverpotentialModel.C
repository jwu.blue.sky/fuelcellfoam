/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::ohmicOverpotentialModel

Description
    A namespace for various ohmic overpotential implementations.

\*---------------------------------------------------------------------------*/

#include "ohmicOverpotentialModel.H"
#include "volFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(ohmicOverpotentialModel, 0);
    defineRunTimeSelectionTable(ohmicOverpotentialModel, dictionary);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::ohmicOverpotentialModel::ohmicOverpotentialModel
(
    const fvMesh& mesh,
    const patchDatabase& pm,
    const dictionary& dict
)
:
    mesh_(mesh),
    pm_(pm),
    hE_(dict.lookupOrDefault<dimensionedScalar>("hE", 0.0)),
    R_(dict.lookupOrDefault<dimensionedScalar>("ASR", -1.0)),
    deviation_(dict.lookupOrDefault<scalar>("deviation", 0.0))
{
    // compute thickness of electrolyte as
    // (electrolyte volume)/(average electrode interface area)
    // Using average area of both electrode interfaces accommodates both
    // planar and cylindrical cells.

    if(hE_.value() == 0)
    {
        // anode side area
        scalar anodeSide = Foam::gSum
        (
            mesh_.magSf().boundaryField()[pm_.electrolyteAnodeID()]
        );

        // cathode side area
        scalar cathodeSide = Foam::gSum
        (
            mesh_.magSf().boundaryField()[pm_.electrolyteCathodeID()]
        );

        // volume
        scalar electrolyteVolume = Foam::gSum
        (
            mesh_.V()
        );

        // thickness value
        hE_.value() = electrolyteVolume/(0.5*(anodeSide + cathodeSide));

        Info<< "    Electrolyte thickness (calculated) hE = " << hE_ << endl;
    }
    else
    {
        Info<< "Electrolyte thickness (read from file) hE = " << hE_ 
	    << nl << endl;
    }

    if (deviation_ == 0)
    {
	Info<< "Warning: ASR deviation zero. Are you using a constant ASR?" << endl;
    }
}


// * * * * * * * * * * * * * * * * Destructors * * * * * * * * * * * * * * * //

Foam::ohmicOverpotentialModel::~ohmicOverpotentialModel()
{}

// ************************************************************************* //
