// Calculation time of the solver Usefull for the Cluster
const scalar calcTime =  60*60*runTime.controlDict().lookupOrDefault<scalar>("calcTime", 0);

// Define stop file for stopping the job
fileName abortName(args.rootPath()/args.globalCaseName()/"stop");

// All times are written in a file
fileName timeFile(args.rootPath()/args.globalCaseName()/"time");
OFstream tf(timeFile, OFstream::ASCII, OFstream::UNCOMPRESSED, OFstream::APPEND);

runTime.setOutputTime();
