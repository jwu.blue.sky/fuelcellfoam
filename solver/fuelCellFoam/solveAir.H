{
    Info << nl << "Solving air flow" << endl;

    pdAir.storePrevIter(); // needed for relaxation

    volScalarField eAir(matAir.epsilon0()*(1.0 - sAir));
    surfaceScalarField eAirf(fvc::interpolate(eAir));

    fvVectorMatrix UEqn
    (
        fvm::div(eAirf*phiAir, Uair, "div(phi,U)")
      - fvm::laplacian(eAir*matAir.mu(), Uair, "laplacian(mu,U)")
    );

    matAir.porousZones().addResistance(eAir, Foam::pow(1. - sAir, 4), UEqn);

    UEqn.relax();

    if(pimple.momentumPredictor())
    {
        solve
        (
            UEqn
          ==
            fvc::reconstruct
            (
	        eAirf*
                (
                  - ghfAir*fvc::snGrad(matAir.rho())
                  - fvc::snGrad(pdAir)
                )*airMesh.magSf()
            )
        );
    }

    while (pimple.correct())
    {
	volScalarField rAU(1.0/UEqn.A());
	surfaceScalarField rhorAUf("rhorAUf", fvc::interpolate(matAir.rho()*rAU));
	volVectorField HbyA(constrainHbyA(rAU*UEqn.H(), Uair, pdAir));

	surfaceScalarField phig(-eAirf*rhorAUf*ghfAir*fvc::snGrad(matAir.rho())*airMesh.magSf());

	surfaceScalarField phiHbyA
	(
	    "phiHbyA",
	    fvc::flux(matAir.rho()*HbyA)
	);

        adjustPhi(phiHbyA, Uair, pdAir);

        phiHbyA += phig;

	constrainPressure(pdAir, matAir.rho(), Uair, phiHbyA, rhorAUf);

        while (pimple.correctNonOrthogonal())
        {
            fvScalarMatrix pdEqn
            (
                fvm::laplacian(eAirf*eAirf*rhorAUf, pdAir,"laplacian(rhorAUf,pd)") 
             == fvc::div(eAirf*phiHbyA) + phaseChangeAir.Svl()
            );

            pdEqn.setReference(pAirRefCell, pAirRefValue);
            pdEqn.solve();

            if (pimple.finalNonOrthogonalIter())
            {
	        phiAir = phiHbyA - pdEqn.flux();

                pdAir.relax();

	        Uair = HbyA + rAU*fvc::reconstruct((phig - pdEqn.flux())/rhorAUf);
                Uair.correctBoundaryConditions();
            }
        }
        pAir = pdAir + matAir.rho()*ghAir;

        continuityErrs(phiAir);

        if (pdAir.needReference())
        {
            pAir += dimensionedScalar
            (
                "p",
                pAir.dimensions(),
                pAirRefValue - getRefCellValue(pAir, pAirRefCell)
            );
            pdAir = pAir - matAir.rho()*ghAir;
        }
    }
    lim.check(Uair, -10, 10);
    lim.check(pdAir, 0, 3e6);
}
